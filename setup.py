from setuptools import setup, find_packages

setup(
    name='model gitflix',
    version='0.1',
    description='Models for gitflix demo models',
    packages=find_packages(),
    package_data={'models': ['*.m5o']},
    install_requires=[],
)
